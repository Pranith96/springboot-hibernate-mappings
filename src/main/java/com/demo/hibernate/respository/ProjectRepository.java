package com.demo.hibernate.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.hibernate.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer>{

}
