package com.demo.hibernate.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.demo.hibernate.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	
	@Query("update Employee e set e.projectId = :projectId where e.employeeId = :employeeId")
	void updatePorjectEmployeeDetails(@Param("projectId") Integer projectId, @Param("employeeId") Integer employeeid);
}
