package com.demo.hibernate.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.hibernate.entity.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {

}