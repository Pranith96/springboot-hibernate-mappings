package com.demo.hibernate.dto;

import com.demo.hibernate.entity.Address;
import com.demo.hibernate.entity.Company;
import com.demo.hibernate.entity.Employee;

public class EmployeeDto1 {

	private Employee employee;
	private Address address;
	private Company company;

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
