package com.demo.hibernate.dto;

import com.demo.hibernate.entity.Status;

public class EmployeeDto {

	private Integer employeeId;
	private String employeeName;
	private String mobileNumber;
	private String email;
	private Integer addressId;
	private String streetNo;
	private String area;
	private String pincode;
	private String state;
	private String country;
	private Integer companyId;
	private Integer cid;
	private String companyName;
	private String companyAddress;
	private Status status;

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public EmployeeDto(Integer employeeId, String employeeName, String mobileNumber, String email, Integer addressId,
			String streetNo, String area, String pincode, String state, String country, Integer companyId, Integer cid,
			String companyName, String companyAddress, Status status) {
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.addressId = addressId;
		this.streetNo = streetNo;
		this.area = area;
		this.pincode = pincode;
		this.state = state;
		this.country = country;
		this.companyId = companyId;
		this.cid = cid;
		this.companyName = companyName;
		this.companyAddress = companyAddress;
		this.status = status;
	}

	public EmployeeDto() {
	}

}
