package com.demo.hibernate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.hibernate.entity.Project;
import com.demo.hibernate.service.ProjectService;

@RestController
@RequestMapping("/api")
public class ProjectController {

	@Autowired
	ProjectService projectService;
	
	@PostMapping("/project/save")
	public ResponseEntity<String> createProject(@RequestBody Project project){
		String projectResponse  = projectService.createProject(project);
		return ResponseEntity.status(HttpStatus.CREATED).body(projectResponse);
		
	}
}
