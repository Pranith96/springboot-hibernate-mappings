package com.demo.hibernate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.hibernate.dto.EmployeeDto;
import com.demo.hibernate.entity.Employee;
import com.demo.hibernate.service.EmployeeService;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

	@Autowired
	EmployeeService service;

	@PostMapping("/save")
	public ResponseEntity<String> createEmployee(@RequestBody Employee employee) throws Exception {
		Employee employeeResponse = service.create(employee);
		return ResponseEntity.status(HttpStatus.CREATED).body("Employee Created Successfully");
	}

	@GetMapping("/get/Employee/{employeeId}")
	public ResponseEntity<EmployeeDto> getEmployee(@PathVariable("employeeId") Integer employeeId) throws Exception {
		EmployeeDto employeeResponse = service.getEmployee(employeeId);
		return ResponseEntity.status(HttpStatus.OK).body(employeeResponse);
	}

	@PutMapping("/update/Employee/project/{employeeId}/{projectId}")
	public ResponseEntity<String> createEmployeeProject(@RequestParam("employeeId") Integer employeeId,
			@RequestParam("projectId") Integer projectId) throws Exception {
		String employeeResponse = service.createEmployeeProject(employeeId, projectId);
		return ResponseEntity.status(HttpStatus.OK).body(employeeResponse);
	}
}
