package com.demo.hibernate.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Table(name = "project_employeetable")
@Entity
public class EmployeeProject implements Serializable {

	private static final long serialVersionUID = 1L;
	/*
	 * You can map that with an embeddable that represents the primary key and
	 * 2 @MapsId annotations. The annotations tell Hibernate to which attribute of
	 * the embeddable it shall assign the primary key value of the associated entity
	 */
	@EmbeddedId
	private EmployeeProjectKey employeeProjectId;

	@ManyToOne
	@MapsId("employeeId")
	@JoinColumn(name = "employee_id")
	private Employee employee;

	@ManyToOne
	@JoinColumn(name = "project_id")
	@MapsId("projectId")
	private Project project;

	public EmployeeProject(EmployeeProjectKey employeeProjectId, Employee employee, Project project) {
		this.employeeProjectId = employeeProjectId;
		this.employee = employee;
		this.project = project;
	}

	public EmployeeProject() {
	}

	public EmployeeProjectKey getEmployeeProjectId() {
		return employeeProjectId;
	}

	public void setEmployeeProjectId(EmployeeProjectKey employeeProjectId) {
		this.employeeProjectId = employeeProjectId;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}
