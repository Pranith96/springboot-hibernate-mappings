package com.demo.hibernate.entity;

public enum Status {

	ACTIVE("ACTIVE"), INACTIVE("INACTIVE");

	private String code;

	Status(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

}
