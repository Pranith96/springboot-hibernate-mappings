package com.demo.hibernate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.hibernate.entity.Project;
import com.demo.hibernate.respository.ProjectRepository;

@Service
public class ProjectServiceImpl implements ProjectService{

	@Autowired
	ProjectRepository projectRepository;
	
	@Override
	public String createProject(Project project) {
		projectRepository.save(project);
		return "Project Saved";
	}

}
