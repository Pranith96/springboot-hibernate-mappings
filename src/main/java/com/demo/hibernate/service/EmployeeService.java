package com.demo.hibernate.service;

import com.demo.hibernate.dto.EmployeeDto;
import com.demo.hibernate.entity.Employee;

public interface EmployeeService {

	Employee create(Employee employee) throws Exception;

	EmployeeDto getEmployee(Integer employeeId) throws Exception;

	String createEmployeeProject(Integer employeeId, Integer projectId) throws Exception;

}
