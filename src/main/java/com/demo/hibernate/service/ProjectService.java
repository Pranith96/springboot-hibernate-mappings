package com.demo.hibernate.service;

import com.demo.hibernate.entity.Project;

public interface ProjectService {

	String createProject(Project project);

}
