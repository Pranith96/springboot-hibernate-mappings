package com.demo.hibernate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.hibernate.dto.EmployeeDto;
import com.demo.hibernate.entity.Employee;
import com.demo.hibernate.entity.EmployeeProject;
import com.demo.hibernate.entity.Project;
import com.demo.hibernate.entity.Status;
import com.demo.hibernate.respository.EmployeeProjectRepository;
import com.demo.hibernate.respository.EmployeeRepository;
import com.demo.hibernate.respository.ProjectRepository;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	EmployeeProjectRepository employeeProjectRepositoty;

	@Override
	public Employee create(Employee employee) throws Exception {
		employee.getAddress().setEmployee(employee);
		employee.setEmployeeStatus(Status.ACTIVE);
		Employee response = employeeRepository.save(employee);
		if (response == null) {
			throw new Exception("Data not saved");
		}
		return response;
	}

	@Override
	public EmployeeDto getEmployee(Integer employeeId) throws Exception {

		Optional<Employee> employee = employeeRepository.findById(employeeId);
		if (!employee.isPresent()) {
			throw new Exception("Data not found");
		}
		EmployeeDto dto = new EmployeeDto();
		dto.setEmail(employee.get().getEmail());
		dto.setAddressId(employeeId);
		dto.setArea(employee.get().getAddress().getArea());
		dto.setCid(employeeId);
		dto.setCompanyAddress(employee.get().getCompany().getCompanyAddress());
		dto.setCompanyId(employeeId);
		dto.setCompanyName(employee.get().getCompany().getCompanyName());
		dto.setCountry(employee.get().getAddress().getCountry());
		dto.setEmployeeId(employeeId);
		dto.setEmployeeName(employee.get().getEmployeeName());
		dto.setMobileNumber(employee.get().getMobileNumber());
		dto.setPincode(employee.get().getAddress().getPincode());
		dto.setState(employee.get().getAddress().getState());
		dto.setStreetNo(employee.get().getAddress().getStreetNo());
		dto.setStatus(employee.get().getEmployeeStatus());
		return dto;
	}

	@Override
	public String createEmployeeProject(Integer employeeId, Integer projectId) throws Exception {
		Optional<Employee> employee = employeeRepository.findById(employeeId);
		if (!employee.isPresent()) {
			throw new Exception("Employee Data not found");
		}
		Optional<Project> projectResponse = projectRepository.findById(projectId);
		if (!projectResponse.isPresent()) {
			throw new Exception("Project Data Not Found");
		}

		for (EmployeeProject empProject : employee.get().getEmployeeProject()) {
			if (empProject.getProject().getProjectId() != projectId) {
				EmployeeProject employeeProject = new EmployeeProject();
				employeeProject.setEmployee(employee.get());
				employeeProject.setProject(empProject.getProject());
				employeeProjectRepositoty.save(employeeProject);
				break;
			} else {
				throw new Exception("Project already assigned to user");
			}
		}
		return "Employee Poject Added Successfully";
	}
}
